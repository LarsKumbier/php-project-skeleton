<?php

namespace Kits;

class MockingBirdTest extends \PHPUnit_Framework_TestCase
{
	/**
	 * @group unit
	 * @small
	 */
	public function test_Mocks()
	{
		$input = "chirp tweet chirp";
		$mb = new MockingBird();
		$expected = $input;
		
		$actual = $mb->mock($input);
		
		$this->assertEquals($expected, $actual, "MockingBird should mock");
	}
}