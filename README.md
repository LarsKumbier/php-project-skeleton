PHP Project Skeleton
====================
Author: Lars Kumbier <lars@kumbier.it>

This is a skeleton for a project to use

* php 5.3
* phpunit for testing
* composer for dependency management
* git for version control


Requirements
============
- php 5.3+
- composer (http://getcomposer.org)
- all other requirements are installed via composer


Installation
============
- clone repository (with --depth=1)
- run `composer update` to install all required files


Usage
=====
Change this file to meet your project


Troubleshooting
===============
Issue Tracking is done on bitbucket


Directory Structure
===================
- `docs/`     Documentation
- `lib/`      Application Libraries
- `tests/`    PHPunit tests
- `tmp/`      A temporary folder, e.g. for code coverage reports
- `vendor/`   automatically filled via `composer update`